const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const randomColor = require('randomcolor');
const createCooldown = require('./create-cooldown');

const app = express();

app.use(express.static(`${__dirname}/../client`));

const server = http.createServer(app);
const io = socketio(server);

let playerNumber = 0;

const users = ['user1', 'user2', 'user3', 'user4', 'user5', 'user6', 'user7'];

let stop = false;

let currentUsers = users;

io.on('connection', (sock) => {

  if(playerNumber < 7) {

    let user = currentUsers.shift();

    const color = randomColor();
    playerNumber++;

    sock.emit('setName', user);
    sock.emit('setColor', color);
    sock.emit('setAudio', 'http://localhost:8080/sounds/' + user + '.mp3');
    const cooldown = createCooldown(5000);
    sock.on('message', (text) => io.emit('message', text));

    sock.on('press', function() {
      if (cooldown() && !stop) {
        stop = true;
        sock.emit('playAudio');
        sock.emit('message', 'You Won!');
        setTimeout(function() {
          stop = false;
          io.emit('message', 'New Round');
        }, 6000)
      }

    });
    sock.conn.on("close", (reason) => {
      playerNumber--;
      currentUsers.unshift(user);
      // called when the underlying connection is closed
    });
  } else {

  }

});

server.on('error', (err) => {
  console.error(err);
});

server.listen(8080, () => {
  console.log('server is ready');
});
