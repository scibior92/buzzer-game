const log = (text) => {
  const parent = document.querySelector('#events');
  const el = document.createElement('li');
  el.innerHTML = text;
  parent.appendChild(el);
  parent.scrollTop = parent.scrollHeight;
};

const setName = (text) => {
  const el = document.querySelector('#user');
  el.innerHTML = text;

};
const setColor = (color) => {
  let el = document.querySelector('#colorSquare');
  el.style.backgroundColor = color;
  el = document.querySelector('#buzzer');
  el.style.backgroundColor = color;
};
const setAudio = (sourceAudio) => {
  var audio = document.getElementById('audio');
  var source = document.getElementById('audioSource');
  source.src = sourceAudio;
  audio.load(); //call this to just preload the audio without playing
};
const playAudio = () => {
  var audio = document.getElementById('audio');
  audio.play(); //call this to play the song right away
  startConfetti();
  setTimeout(function() {
    stopConfetti();
  }, 3000)
};
const onChatSubmitted = (sock) => (e) => {
  e.preventDefault();

  const input = document.querySelector('#chat');
  const text = input.value;
  input.value = '';

  sock.emit('message', text);
};


(() => {

  const buzzer = document.getElementById('buzzer');
  const sock = io();

  const onClickBuzzer = (e) => {
    sock.emit('press');
    sock.on('playAudio', playAudio);
  };

  sock.on('setName', setName);
  sock.on('setColor', setColor);
  sock.on('setAudio', setAudio);
  sock.on('message', log);

  document
    .querySelector('#chat-form')
    .addEventListener('submit', onChatSubmitted(sock));
  buzzer.addEventListener('click', onClickBuzzer);
})();
